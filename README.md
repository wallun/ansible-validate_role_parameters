# validate_role_parameters

This role **MUST** be used as a depencendy from **ANOTHER** role.

*validate_role_parameters* checks if the variables used by a role **and**
defined in a JSON schema meet their requirements.

# Example Playbook

```yaml
---
- name: Validate role parameters
  hosts: localhost
  roles:
    # A role that uses validate_role_parameters
    - role: my_role
```

# Example Role

```yaml
---
# default/main.yaml
role_example_string: world
role_example_integer: foobar
---
# meta/main.yaml
dependencies:
  - name: validate_role_parameters
    src:
      git+https://gitlab.archlinux.org/wallun/ansible-validate_role_parameters
    validate_role_parameters_role: my_role # validation won't work without
---
# tasks/main.yaml
- name: Validate role parameters
  ansible.builtin.debug:
    msg: "hello {{ role_example_string }}"
---
# vars/main.yaml
role_name_variable_schema:
  type: object
  properties:
    role_example_string:
      type: string
    role_example_integer:
      type: integer
      minimum: 1
      maximum: 65535
```
# License

GPLv3

# Author information

Wallun \<wallun CAT disroot DOG org\>
